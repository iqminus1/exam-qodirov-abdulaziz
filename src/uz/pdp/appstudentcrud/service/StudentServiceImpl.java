package uz.pdp.appstudentcrud.service;

import uz.pdp.appstudentcrud.payload.StudentDTO;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.stream.Collectors;

public class StudentServiceImpl implements StudentService {

    private static ReentrantLock lock = new ReentrantLock();

    private StudentServiceImpl() {
        logger = Logger.getLogger("Student Service");
        try {
            FileHandler fileHandler = new FileHandler("logger/log.txt",true);
            fileHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(fileHandler);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static StudentServiceImpl instance;

    public static StudentServiceImpl getInstance() {
        if (instance == null) {
            lock.lock();
            if (instance == null) instance = new StudentServiceImpl();
            lock.unlock();
        }
        return instance;
    }

    private List<StudentDTO> studentDTOS = Collections.synchronizedList(new ArrayList<>());
    private Logger logger;

    @Override
    public List<StudentDTO> getByGroup(Integer groupId) {
        logger.log(Level.INFO, "getByGroup method ishladi");
        return studentDTOS.stream()
                .filter(s -> s.getGroupDTO().getId().equals(groupId))
                .collect(Collectors.toList());
    }

    @Override
    public StudentDTO getById(Integer id) {
        logger.log(Level.INFO, "getById method ishladi");
        return getByIdOrElseThrow(id);
    }

    @Override
    public StudentDTO add(StudentDTO studentDTO) {
        logger.log(Level.INFO, "add method ishladi");
        studentDTO.setId(studentDTOS.size() + 1);
        studentDTOS.add(studentDTO);
        return studentDTO;
    }

    @Override
    public StudentDTO edit(Integer id, StudentDTO studentDTO) {
        logger.log(Level.INFO, "edit method ishladi");
        StudentDTO newStudent = getByIdOrElseThrow(id);

        newStudent.setAddressDTO(studentDTO.getAddressDTO());
        newStudent.setGroupDTO(studentDTO.getGroupDTO());
        newStudent.setBirthDate(studentDTO.getBirthDate());
        newStudent.setFirstName(studentDTO.getFirstName());
        newStudent.setLastName(studentDTO.getLastName());
        newStudent.setPhoneNumber(studentDTO.getPhoneNumber());
        newStudent.setBiographyFilePath(studentDTO.getBiographyFilePath());

        return newStudent;
    }

    @Override
    public boolean delete(Integer id) {
        logger.log(Level.INFO, "delete method ishladi");
        return studentDTOS.remove(getByIdOrElseThrow(id));
    }

    @Override
    public String read(Integer id) {
        StudentDTO student = getByIdOrElseThrow(id);
        try (InputStream input = new FileInputStream(student.getBiographyFilePath())) {
            byte[] bytes = input.readAllBytes();
            logger.log(Level.INFO, "read method ishladi");
            return new String(bytes);
        } catch (IOException e) {
            logger.log(Level.INFO, "read methoda xatolikka tushdi");
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean serialize() {
        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("db/students.txt"))) {
            output.writeObject(studentDTOS);
            logger.log(Level.INFO, "serialize method ishladi");
            return true;
        } catch (IOException e) {
            logger.log(Level.INFO, "serialize methoda xatolikka tushdi");
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean deserialize() {
        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream("db/students.txt"))) {
            studentDTOS = (List<StudentDTO>) input.readObject();
            logger.log(Level.INFO, "deserialize method ishladi");
            return true;
        } catch (IOException | ClassNotFoundException e) {
            logger.log(Level.INFO, "deserialize methoda xatolikka tushdi");
            throw new RuntimeException(e);
        }
    }

    private StudentDTO getByIdOrElseThrow(Integer id) {
        return studentDTOS.stream()
                .filter(s -> s.getId().equals(id))
                .findFirst()
                .orElseThrow();
    }
}
