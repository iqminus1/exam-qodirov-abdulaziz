package uz.pdp.appstudentcrud.service;

import uz.pdp.appstudentcrud.payload.GroupDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class GroupServiceImpl implements GroupService{
    private static ReentrantLock lock = new ReentrantLock();

    private GroupServiceImpl() {
    }

    private static GroupServiceImpl instance;

    public static GroupServiceImpl getInstance() {
        if (instance == null) {
            lock.lock();
            if (instance == null) instance = new GroupServiceImpl();
            lock.unlock();
        }
        return instance;
    }
    List<GroupDTO> groupDTOS = Collections.synchronizedList(new ArrayList<>());
    @Override
    public List<GroupDTO> all() {
        return groupDTOS;
    }

    @Override
    public GroupDTO add(GroupDTO groupDTO) {
        groupDTO.setId(groupDTOS.size());
        groupDTOS.add(groupDTO);
        return groupDTO;
    }

    @Override
    public GroupDTO edit(Integer id, GroupDTO groupDTO) {
        GroupDTO newGroupDTO = getByIdOrElseThrow(id);
        newGroupDTO.setName(groupDTO.getName());

        return newGroupDTO;
    }

    @Override
    public boolean delete(Integer id) {
        return groupDTOS.remove(getByIdOrElseThrow(id));
    }

    @Override
    public GroupDTO getByIdOrElseThrow(Integer id) {
        return groupDTOS.stream()
                .filter(g -> g.getId().equals(id))
                .findFirst()
                .orElseThrow();
    }
}
