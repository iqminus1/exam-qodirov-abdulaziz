package uz.pdp.appstudentcrud;

import uz.pdp.appstudentcrud.payload.AddressDTO;
import uz.pdp.appstudentcrud.payload.GroupDTO;
import uz.pdp.appstudentcrud.payload.StudentDTO;
import uz.pdp.appstudentcrud.service.AddressServiceImpl;
import uz.pdp.appstudentcrud.service.GroupService;
import uz.pdp.appstudentcrud.service.GroupServiceImpl;
import uz.pdp.appstudentcrud.service.StudentServiceImpl;

import java.time.LocalDate;
import java.util.List;

public class App {
    private static StudentServiceImpl studentService = StudentServiceImpl.getInstance();
    private static GroupServiceImpl groupService = GroupServiceImpl.getInstance();
    private static AddressServiceImpl addressService = AddressServiceImpl.getInstance();

    public static void main(String[] args) {
        System.out.println(studentService.deserialize());

//        System.out.println(studentService.read(4));
//
//        System.out.println(studentService.getById(3));

      /*  addGroup();
        addAddress();
        addStudent();
        System.out.println(studentService.getByGroup(4));
        System.out.println(studentService.delete(4));
        System.out.println(studentService.edit(3, new StudentDTO(null,
                "Name ->" + 35,
                "Surname ->" + 35,
                "Name ->" + 35,
                LocalDate.now(),
                new GroupDTO(14, "name"),
                new AddressDTO(14, "name", "city", "addressline"),
                "content/biography.txt")));*/

        /*addGroup();
        addAddress();
        addStudent();
        System.out.println(studentService.edit(2, new StudentDTO(null,
                "Name ->" + 35,
                "Surname ->" + 35,
                "Name ->" + 35,
                LocalDate.now(),
                groupService.getByIdOrElseThrow(4),
                addressService.getByIdOrElseThrow(4),
                "content/biography.txt")));*/
//        System.out.println(studentService.serialize());
    }

    private static void addStudent() {
        List<GroupDTO> allGroup = groupService.all();
        List<AddressDTO> allAddress = addressService.all();

        for (int i = 0; i < 100; i++) {
            studentService.add(new StudentDTO(null,
                    "Name ->" + i,
                    "Surname ->" + i,
                    "Name ->" + i,
                    LocalDate.now(),
                    allGroup.get(i % allGroup.size()),
                    allAddress.get(i % allAddress.size()),
                    "content/biography.txt"));
        }
    }

    private static void addAddress() {
        for (int i = 0; i < 20; i++) {
            addressService.add(new AddressDTO(null,
                    "Region -> " + i,
                    "City - >" + i,
                    "AddressLine -> " + i));
        }
    }

    private static void addGroup() {
        for (int i = 0; i < 20; i++) {
            groupService.add(new GroupDTO(null, "Group name -> " + i));
        }
    }

}
